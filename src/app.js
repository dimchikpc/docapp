import Vue from 'vue'
import App from './components/App.vue'
import { BootstrapVue } from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const app = new Vue({
  render: h => h(App)
}).$mount('#app');