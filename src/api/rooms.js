import axios from 'axios'

const BASE_URL = 'http://localhost:9000/api'

export default axios.get(`${BASE_URL}/rooms.json`)